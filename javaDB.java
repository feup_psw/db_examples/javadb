
/* NOTE: To run this program, you first need to 
 *        DOWNLOAD the PostgreSQL JDBC driver!!
 * 
 * 
 * Details taken from...
 *  https://www.postgresqltutorial.com/postgresql-jdbc/connecting-to-postgresql-database/
 * 
 * "To connect to the PostgreSQL database server from a Java program, 
 * you need to have PostgreSQL JDBC driver. You can download the latest 
 * version of the driver on the postgresql.org website via the download page.
 * https://jdbc.postgresql.org/download/
 * 
 * The downloaded file is a jar file. You should copy it to a specific
 * folder e.g. /javaDB/lib so that you can remember its location and 
 * be able to add it to your Java application later."
 */


import java.sql.*;

public class javaDB {
    /* syntax for DB_URL:         jdbc:postgresql://<database_host>:<port>/<database_name> */
    static final String db_url = "jdbc:postgresql://10.227.240.130:5432/pswa0609";
    static final String user   = "pswa0609";
    static final String passwd = "uTmmIxtj";
    static final String query  = "SELECT id, matricula, tipo FROM testing.carro";
 
    public static void main(String[] args) {
           // Connect to the DBMS (DataBase Management Server)
       try(Connection conn = DriverManager.getConnection(db_url, user, passwd);
          // Execute an SQL statement
          Statement stmt = conn.createStatement();
          ResultSet rs = stmt.executeQuery(query);) {
          // Analyse the resulting data
          while (rs.next()) {
             System.out.print("ID: "          + rs.getInt("id"));
             System.out.print(", Matricula: " + rs.getString("matricula"));
             System.out.print(", Tipo: "      + rs.getString("tipo"));
             System.out.println();
          }
       } catch (SQLException e) {
          e.printStackTrace();
       } 
    }
}
